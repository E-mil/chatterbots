module Chatterbot where
import Utilities
import System.Random
import Data.Char
import Data.Maybe

chatterbot :: String -> [(String, [String])] -> IO ()
chatterbot botName botRules = do

    let temp1 = singleWildcardMatch "*do*" "bdod"
    let temp2 = longerWildcardMatch "*do" "dobedo"
    let temp3 = match '*' "* and *" "you and me"
    let temp4 = transformationApply '*' (\x -> x) "My name is Zacharias" ("My name is *", "Je m'appelle *")
    let temp5 = transformationsApply '*' id [("My name is *", "Je m'appelle *"),("My name is *", "Mitt namn är *")] "My name is Zacharias"
    let temp6 = transformationsApply '*' id (reverse [("My name is *", "Je m'appelle *"),("My name is *", "Mitt namn är *")]) "My name is Zacharias"
    let temp7 = reflect ["i", "will", "never", "see", "my","reflection", "in", "your", "eyes"]
    let temp8 = rulesApply [(words "my", words "your"), (words "i", words "you"), (words "your", words "my")] ["i", "will", "never", "see", "my","reflection", "in", "your", "eyes"]
    let temp9 = rulesApply [(words "",words "Speak up! I can't hear you."),(words "I need *",words "Why do you need * ?"),(words "*",words "Please tell me more.")] (words "I need a monkey")
    let temp10 = rulesCompile [("",["Speak up! I can't hear you.","GAAH!"]),("I need *",["Why do you need * ?","Would it really help you to get * ?","Are you sure you need * ?"])]
    r <- randomIO :: IO Float
    putStrLn ("You rolled " ++ show (floor (6*r+1)))
    putStrLn $ show temp1
    putStrLn $ show temp2
    putStrLn $ show temp3
    putStrLn $ show temp4
    putStrLn $ "Franska: " ++ show temp5
    putStrLn $ "Svenska: " ++ show temp6
    putStrLn $ show temp7
    putStrLn $ show temp8
    putStrLn $ show temp9
    putStrLn $ show temp10

    putStrLn ("\n\nHi! I am " ++ botName ++ ". How are you?")
    botloop
  where
    brain = rulesCompile botRules
    botloop = do
      putStr "\n: "
      question <- getLine
      answer <- stateOfMind brain
      putStrLn (botName ++ ": " ++ (present . answer . prepare) question)
      if (not . endOfDialog) question then botloop else return ()

--------------------------------------------------------

type Phrase = [String]
type PhrasePair = (Phrase, Phrase)
type BotBrain = [(Phrase, [Phrase])]


--------------------------------------------------------

stateOfMind :: BotBrain -> IO (Phrase -> Phrase)
stateOfMind x = do
    r <- randomIO :: IO Float
    return (\y -> rulesApply (map (\z -> (fst z, pick r $ snd z)) x) $ y)
{- DONE ?? -}
    
rulesApply :: [PhrasePair] -> Phrase -> Phrase
rulesApply patterns x = fromMaybe [] $ transformationsApply "*" reflect patterns x

reflect :: Phrase -> Phrase
reflect = map $ try $ flip  lookup reflections
{- DONE ?? -}

--point free fix
--reflect [] = []
--reflect [x] = [replaceWord x reflections]
--reflect (x:xs) = (replaceWord x reflections):(reflect xs)
--Own method
--replaceWord :: String -> [([Char], [Char])] -> String
--replaceWord s [] = s
--replaceWord s p 
--    | s == fst (head p) = snd $ head p
--    | otherwise = replaceWord s (tail p)

reflections =
  [ ("am",     "are"),
    ("was",    "were"),
    ("i",      "you"),
    ("i'm",    "you are"),
    ("i'd",    "you would"),
    ("i've",   "you have"),
    ("i'll",   "you will"),
    ("my",     "your"),
    ("me",     "you"),
    ("are",    "am"),
    ("you're", "i am"),
    ("you've", "i have"),
    ("you'll", "i will"),
    ("your",   "my"),
    ("yours",  "mine"),
    ("you",    "me")
  ]


---------------------------------------------------------------------------------

endOfDialog :: String -> Bool
endOfDialog = (=="quit") . map toLower

present :: Phrase -> String
present = unwords

prepare :: String -> Phrase
prepare = reduce . words . map toLower . filter (not . flip elem ".,:;*!#%&|") 

rulesCompile :: [(String, [String])] -> BotBrain
rulesCompile = map (\y -> (words . map toLower . fst $ y, map (words . map toLower) . snd $ y))
{- DONE -}


--------------------------------------


reductions :: [PhrasePair]
reductions = (map.map2) (words, words)
  [ ( "please *", "*" ),
    ( "can you *", "*" ),
    ( "could you *", "*" ),
    ( "tell me if you are *", "are you *" ),
    ( "tell me who * is", "who is *" ),
    ( "tell me what * is", "what is *" ),
    ( "do you know who * is", "who is *" ),
    ( "do you know what * is", "what is *" ),
    ( "are you very *", "are you *" ),
    ( "i am very *", "i am *" ),
    ( "hi *", "hello *")
  ]

reduce :: Phrase -> Phrase
reduce = reductionsApply reductions

reductionsApply :: [PhrasePair] -> Phrase -> Phrase
reductionsApply = fix . try . transformationsApply "*" id
--point free fix
--reductionsApply pairs phrase
--    | phrase' == [] =  phrase
--    | otherwise = reductionsApply pairs phrase'
--        where phrase' = fromMaybe [] $ transformationsApply "*" id pairs phrase
{- DONE ?? -}

-------------------------------------------------------
-- Match and substitute
--------------------------------------------------------

-- Replaces a wildcard in a list with the list given as the third argument
substitute :: Eq a => a -> [a] -> [a] -> [a]
substitute _ [] _ = []
substitute wildcard (t:ts) s  
    | wildcard == t = s ++ substitute wildcard ts s
    | otherwise = [t] ++ substitute wildcard ts s
{- DONE ?? -}


-- Tries to match two lists. If they match, the result consists of the sublist
-- bound to the wildcard in the pattern list.
match :: Eq a => a -> [a] -> [a] -> Maybe [a]
match _ [] [] = Just []
match _ [] s = Nothing
match wildcard p [] = if p == [wildcard] then Just [] else Nothing
match wildcard (p:ps) (s:ss)  
    | p /= wildcard = if p == s then match wildcard ps ss else Nothing
    | otherwise = (orElse $ singleWildcardMatch (p:ps) (s:ss)) $ longerWildcardMatch (p:ps) (s:ss)
{- DONE ?? -}


-- Helper function to match
singleWildcardMatch, longerWildcardMatch :: Eq a => [a] -> [a] -> Maybe [a]
singleWildcardMatch (wc:ps) (x:xs) = maybe Nothing (\y -> Just [x]) $ match wc ps xs
{- DONE ?? -}
longerWildcardMatch (wc:ps) (x:xs) = maybe Nothing (\y -> Just $ [x] ++ y) $ match wc ([wc] ++ ps) xs
{- DONE ?? -}



-- Test cases --------------------

testPattern =  "a=*;"
testSubstitutions = "32"
testString = "a=32;"

substituteTest = substitute '*' testPattern testSubstitutions
substituteCheck = substituteTest == testString

matchTest = match '*' testPattern testString
matchCheck = matchTest == Just testSubstitutions



-------------------------------------------------------
-- Applying patterns
--------------------------------------------------------

-- Applying a single pattern
transformationApply :: Eq a => a -> ([a] -> [a]) -> [a] -> ([a], [a]) -> Maybe [a]
transformationApply wc function list patterns =
    maybe Nothing (Just . substitute wc (snd patterns) . function) $ match wc (fst patterns) list
{- DONE -}


-- Applying a list of patterns until one succeeds
transformationsApply :: Eq a => a -> ([a] -> [a]) -> [([a], [a])] -> [a] -> Maybe [a]
transformationsApply wc function patternsList list
    | isJust res = res
    | length patternsList > 1 = transformationsApply wc function (tail patternsList) list
    | otherwise = Nothing
        where res = transformationApply wc function list (head patternsList)


{- DONE ?? -}


